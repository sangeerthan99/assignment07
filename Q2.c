#include <stdio.h>
int main() {
    char str[1000], a;
    int count = 0;

    printf("Enter a string: ");
    fgets(str, sizeof(str), stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &a);

    for (int x = 0; str[x] != '\0'; ++x) {
        if (a == str[x])
            ++count;
    }

    printf("Frequency of %c = %d", a, count);
    return 0;
}
