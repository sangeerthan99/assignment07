#include <stdio.h>
void MatrixElements(int matrix[][10], int row, int column) {

   printf("\nEnter elements: \n");

   for (int x = 0; x < row; ++x) {
      for (int j = 0; j < column; ++j) {
         printf("Enter a%d%d: ", x + 1, j + 1);
         scanf("%d", &matrix[x][j]);
      }
   }
}
void multiplyMatrixes(int first[][10],
                      int second[][10],
                      int result[][10],
                      int r1, int c1, int r2, int c2) {

   for (int x = 0; x < r1; ++x) {
      for (int j = 0; j < c2; ++j) {
         result[x][j] = 0;
      }
   }

   for (int x = 0; x < r1; ++x) {
      for (int j = 0; j < c2; ++j) {
         for (int k = 0; k < c1; ++k) {
            result[x][j] += first[x][k] * second[k][j];
         }
      }
   }
}
void display(int result[][10], int row, int column) {

   printf("\nOutput Matrix:\n");
   for (int x = 0; x < row; ++x) {
      for (int j = 0; j < column; ++j) {
         printf("%d  ", result[x][j]);
         if (j == column - 1)
            printf("\n");
      }
   }
}

int main() {
   int first[10][10], second[10][10], result[10][10], r1, c1, r2, c2;
   printf("Input rows and column for the first matrix: ");
   scanf("%d %d", &r1, &c1);
   printf("Input rows and column for the second matrix: ");
   scanf("%d %d", &r2, &c2);

   while (c1 != r2) {
      printf("Error! Enter rows and columns again.\n");
      printf("Enter rows and columns for the first matrix: ");
      scanf("%d%d", &r1, &c1);
      printf("Enter rows and columns for the second matrix: ");
      scanf("%d%d", &r2, &c2);
   }

   MatrixElements(first, r1, c1);

   MatrixElements(second, r2, c2);

   multiplyMatrixes(first, second, result, r1, c1, r2, c2);

   display(result, r1, c2);

   return 0;
}
