#include <stdio.h>
void sentencereverse();
int main() {
    printf("Enter a sentence: ");
    sentencereverse();
    return 0;
}

void sentencereverse() {
    char a;
    scanf("%c", &a);
    if (a != '\n') {
        sentencereverse();
        printf("%c", a);
    }
}
